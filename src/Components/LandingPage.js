const LandingPage = () => {
  return (
    <section className="bg-[url('Assets/image2.png')] bg-cover h-screen " >
      <div className="backdrop-brightness-50 w-full h-full flex-col flex items-center justify-center gap-y-1.5">
        <img src="Assets/racc.png" className="border-2 h-[170px] w-[170px] rounded-full border-solid border-white brightness-100"></img>
        <h1 className="text-white font-bold font-sans text-5xl y-4 brightness-100">Jovan Prokin</h1>
        <h3 className="text-white font-bold font-sans text-xl brightness-100">Front-End Developer</h3>
        <p className="text-white text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo <br/> 
        ligula eget dolor. Aenean massa. Cum sociis natoque </p>
        <button className="text-white w-[130px] h-[40px] ">Know more</button>
      </div>        
    </section>
  )
}


export default LandingPage;